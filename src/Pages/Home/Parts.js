import React, { useEffect, useState } from "react";
import Part from "./Part";

const Parts = () => {
  const [parts, setParts] = useState([]);
  useEffect(() => {
    fetch("https://mighty-peak-09246.herokuapp.com/get-parts")
      .then((res) => res.json())
      .then((data) => setParts(data));
  }, []);
  return (
    <div className="section_spacing gray_bg">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className="theme_title text-center mb-4">
              <h3>Our Parts</h3>
            </div>
          </div>
        </div>
        <div className="row">
          {parts.slice(-6).map((item) => (
            <Part item={item} key={item._id}></Part>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Parts;
