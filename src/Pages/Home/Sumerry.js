import React from "react";
import {
  AiOutlineUserAdd,
  AiOutlinePound,
  AiOutlineStar,
} from "react-icons/ai";
const Sumerry = () => {
  return (
    <div className="counter_area">
      <div className="container">
        <div className="row">
          <div className="col-lg-7">
            <div className="counter_wrapper">
              <div className="single_counter">
                <div className="d-flex flex-column icon_box">
                  <AiOutlineUserAdd />
                  <h3>
                    <span className="counter">17</span>k+
                  </h3>
                </div>
                <div className="counter_content">
                  <h4>served customers</h4>
                  <p>
                    Key features are the ability to develop relationships with{" "}
                    <br />
                    customers, caring and kind knowledge offer <br />
                    learner engaging customers of their.
                  </p>
                </div>
              </div>
              <div className="single_counter">
                <div className="d-flex flex-column icon_box">
                  <AiOutlinePound></AiOutlinePound>
                  <h3>
                    {" "}
                    <span className="counter">110</span>+
                  </h3>
                </div>

                <div className="counter_content">
                  <h4>Annual revenue</h4>
                  <p>
                    Key features are the ability to develop relationships with{" "}
                    <br />
                    customers, caring and kind knowledge offer <br />
                    learner engaging customers of their.
                  </p>
                </div>
              </div>
              <div className="single_counter">
                <div className="d-flex flex-column icon_box">
                  <AiOutlineStar></AiOutlineStar>
                  <h3>
                    {" "}
                    <span className="counter">1.2</span>k+
                  </h3>
                </div>

                <div className="counter_content">
                  <h4>Reviews</h4>
                  <p>
                    Key features are the ability to develop relationships with{" "}
                    <br />
                    customers, caring and kind knowledge offer <br />
                    learner engaging customers of their.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Sumerry;
