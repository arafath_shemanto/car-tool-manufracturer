import React from "react";
import Blog from "../Blog/Blog";
import Policy from "../Policy/Policy";
import Banner from "./Banner";
import Review from "./Review";
import Sumerry from "./Sumerry";
import Parts from "./Parts";

const Home = () => {
  return (
    <div>
      <Banner></Banner>
      <Parts></Parts>
      <Sumerry></Sumerry>
      <Policy></Policy>
      <Review></Review>
      <Blog></Blog>
    </div>
  );
};

export default Home;
