import React from "react";

const Banner = () => {
  return (
    <div className="home_banner ">
      <div className="home_banner_single  home_banner_bg_1">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="banner__text">
                <span className="font_14 f_w_700 text-uppercase">
                  NEW TECHNOLOGY BUILD
                </span>
                <h3 className="text-text-capitalize">
                  WHEELS & TIRES COLLECTIONS
                </h3>
                <p className="font_16 f_w_500 text-capitalize text-white">
                  product are now available for you?
                </p>
                <a href="#" className="primary_btn3 min_200">
                  Shop now
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Banner;
