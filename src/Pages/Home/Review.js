import React, { useEffect, useState } from "react";

const Review = () => {
  const [reviws, setreview] = useState([]);
  useEffect(() => {
    const url = `https://mighty-peak-09246.herokuapp.com/add-review`;
    fetch(url)
      .then((res) => res.json())
      .then((data) => setreview(data));
  }, []);
  return (
    <div className="testmonial_area_two">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className="theme_title text-center mb_40">
              <h3>Reviews</h3>
            </div>
          </div>
        </div>
        <div className="row">
          {[...reviws].reverse().map((item, index) => (
            <div key={index} className="col-xl-4">
              <div className="single_testmonial mb_30">
                <p> “{item?.description}”</p>
                <h5>rating: {item?.ratings}</h5>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Review;
