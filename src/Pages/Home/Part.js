import React from "react";
import { useNavigate } from "react-router-dom";

const Part = ({ item }) => {
  const navigate = useNavigate();

  const {
    _id,
    name,
    img,
    description,
    minimum_order,
    available_quantity,
    single_prise,
  } = item;
  const purchageHandeler = () => {
    navigate(`/parchage/${_id}`);
  };
  return (
    <div className="col-xl-3 col-lg-4 col-md-6">
      <div className="product_widget mb_30">
        <div className="thumb">
          <img className="img-fluid" src={img} alt="" />
        </div>
        <div className="product__meta">
          <h4>{name}</h4>
          <p className="mb-2">{description}</p>
          <p className="text-capitalize">
            minimub order quentity : <b>{minimum_order}</b>
          </p>
          <p className="text-capitalize">
            available quentity : <b>{available_quantity}</b>
          </p>
          <p className="text-capitalize mb_30">
            {" "}
            price (per unit price): <b>${single_prise}</b>
          </p>
          <button
            onClick={purchageHandeler}
            className="primary_btn3 small_btn radius_5px"
          >
            purchase now
          </button>
        </div>
      </div>
    </div>
  );
};

export default Part;
