import React from "react";
import { ListGroup } from "react-bootstrap";

const MyPortfolio = () => {
  return (
    <div className="section_spacing">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <h3>My portfolio</h3>
            <h4>Name : Arafath Hossain</h4>
            <h4>email address : arafathh.shemanto@gmail.com</h4>
            <h5>list of technologies </h5>
            <ListGroup>
              <ListGroup.Item>HTML</ListGroup.Item>
              <ListGroup.Item>CSS</ListGroup.Item>
              <ListGroup.Item>SASS</ListGroup.Item>
              <ListGroup.Item>Bootstrap</ListGroup.Item>
              <ListGroup.Item>JS</ListGroup.Item>
              <ListGroup.Item>JQuery</ListGroup.Item>
              <ListGroup.Item>ReactJS</ListGroup.Item>
              <ListGroup.Item>Basic NODE, MONGODB ,EXPRESSJS</ListGroup.Item>
            </ListGroup>
            <h5 className="mt-3 mb-2">live website links</h5>
            <p className="text-capitalize">
              I did many project which is approved in themeforest here is links{" "}
            </p>
            <ListGroup>
              <ListGroup.Item>
                <a href="https://html.uxseven.com/codethemes/ecommerce/infixvuci/index.php">
                  infixvuci ecommerce theme for themeforest
                </a>
              </ListGroup.Item>
              <ListGroup.Item>
                <a href="https://html.uxseven.com/codethemes/maxclean/index.html">
                  maxclean cleaner theme for themeforest
                </a>
              </ListGroup.Item>
              <ListGroup.Item>
                <a href="https://html.uxseven.com/codethemes/anthony/index.html">
                  anthony personal portfolio theme for themeforest
                </a>
              </ListGroup.Item>
            </ListGroup>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MyPortfolio;
