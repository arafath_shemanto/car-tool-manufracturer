import { useEffect } from "react";
import icon1 from "../../assets/img/svg/google_icon.svg";
import loginImg from "../../assets/img/login_img.png";
import { Link, useLocation, useNavigate } from "react-router-dom";
import logo from "../../assets/img/logo.png";
import { useForm } from "react-hook-form";
import {
  useSignInWithEmailAndPassword,
  useSignInWithGoogle,
} from "react-firebase-hooks/auth";
import auth from "../../firebase.init.js";
import useToken from "../../hooks/useToken";

const Login = () => {
  const [signInWithGoogle, gUser, gLoading, gError] = useSignInWithGoogle(auth);
  const {
    register,
    formState: { errors },
    handleSubmit,
  } = useForm();
  const [signInWithEmailAndPassword, user, loading, error] =
    useSignInWithEmailAndPassword(auth);
  const [token] = useToken(gUser || user);
  let location = useLocation();
  const navigate = useNavigate();
  let from = location.state?.from?.pathname || "/";

  useEffect(() => {
    if (token) {
      navigate(from, { replace: true });
    }
  }, [token, from, navigate]);
  let error_msg;
  if (gLoading || loading) {
    return <p>loading</p>;
  }
  if (gError || error) {
    console.log(gError);
    error_msg = (
      <p className="error_text">{gError?.message || error.message}</p>
    );
  }

  const onSubmit = (data) => {
    console.log(data);
    signInWithEmailAndPassword(data.email, data.password);
  };
  return (
    <div className="amazy_login_area">
      <div className="amazy_login_area_left d-flex align-items-center justify-content-center">
        <div className="amazy_login_form">
          <Link to="/" className="logo mb_50 d-block">
            <img src={logo} alt="" />
          </Link>
          <h3 className="m-0">Sign In</h3>
          <p className="support_text">
            See your growth and get consulting support!
          </p>
          <button
            onClick={() => signInWithGoogle()}
            className="google_logIn d-flex align-items-center justify-content-center w-100"
          >
            <img src={icon1} alt="" />
            <h5 className="m-0 font_16 f_w_500">Sign up with Google</h5>
          </button>
          <div className="form_sep2 d-flex align-items-center">
            <span className="sep_line flex-fill"></span>
            <span className="form_sep_text font_14 f_w_500 ">
              or Sign in with Email
            </span>
            <span className="sep_line flex-fill"></span>
          </div>

          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="row">
              <div className="col-12 mb_20">
                <label className="primary_label2">
                  Email Address <span>*</span>{" "}
                </label>
                <input
                  name="name"
                  placeholder="Enter user name or email"
                  className="primary_input3 radius_5px "
                  {...register("email", {
                    required: {
                      value: true,
                      message: "Email is required",
                    },
                    pattern: {
                      value: /[A-Za-z]{3}/,
                      message: "Try a valid email",
                    },
                  })}
                />
                {errors.email?.type === "required" && (
                  <span className="error_text">{errors.email.message}</span>
                )}
                {errors.email?.type === "pattern" && (
                  <span className="error_text">{errors.email.message}</span>
                )}
              </div>
              <div className="col-12 mb_20">
                <label className="primary_label2">
                  Password <span>*</span>{" "}
                </label>
                <input
                  name="name"
                  placeholder="Enter user name or email"
                  className="primary_input3 radius_5px "
                  {...register("password", {
                    required: {
                      value: true,
                      message: "password is required",
                    },
                    minLength: {
                      value: 6,
                      message: "password must be  6 or longer ",
                    },
                  })}
                />
                {errors.password?.type === "required" && (
                  <span className="error_text">{errors.password.message}</span>
                )}
                {errors.password?.type === "minLength" && (
                  <span className="error_text">{errors.password.message}</span>
                )}
              </div>

              <div className="col-12">
                {error_msg}
                <button
                  type="submit"
                  className="primary_btn3 style2 radius_5px  w-100 text-uppercase  text-center mb_25"
                >
                  Sign In
                </button>
              </div>
              <div className="col-12">
                <p className="sign_up_text">
                  Don’t have an Account?
                  <Link to="/register"> Sign Up</Link>
                </p>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div className="amazy_login_area_right d-flex align-items-center justify-content-center">
        <div className="amazy_login_area_right_inner d-flex align-items-center justify-content-center flex-column">
          <div className="thumb">
            <img className="img-fluid" src={loginImg} alt="" />
          </div>
          <div className="login_text d-flex align-items-center justify-content-center flex-column text-center">
            <h4>Turn your ideas into reality.</h4>
            <p className="m-0">
              Consistent quality and experience across all platforms and
              devices.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
