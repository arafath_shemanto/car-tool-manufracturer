import React from "react";

const Blogs = () => {
  return (
    <div className="section_spacing">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className="theme_title mb-4">
              <h3>Blogs</h3>
            </div>
            <div className="col-12">
              <h4>
                Q1. How will you improve the performance of a React Application?
              </h4>
              <p>
                Ans : i can Use an arrow function make code shorter . And can
                improve in performace . i can avoid all the inline and internal
                style and attributes, i can avoid extra tags by using React
                fragments etc
              </p>
            </div>
            <div className="col-12">
              <h4>
                Q2. What are the different ways to manage a state in a React
                application?
              </h4>
              <p>
                Ans : there are many ways . that i can use to manage raect state
                management. here are some example like Global state, Local
                state, Server state, URL state, contextApi ,prop Stateup , props
                drillings
              </p>
            </div>
            <div className="col-12">
              <h4>Q3. How does prototypical inheritance work?</h4>
              <p>
                Ans :its a feature in javascript . javascript used to add
                methods and properties in objects and It is a method by which an
                object can inherit the properties and methods of another object.
                use have to use constructor to use this .
              </p>
            </div>
            <div className="col-12">
              <h4>
                Q4. Why you do not set the state directly in React. For example,
                if you have const [products, setProducts] = useState([]). Why
                you do not set products = [...] instead, you use the setProducts
              </h4>
              <p>
                Ans :i do not set the state directly in React. Because that data
                can be change by any action . and this data can be set
                dynamically . directly set state cannot be work here . so i do
                not set state like that
              </p>
            </div>
            <div className="col-12">
              <h4>Q5.What is a unit test? Why should write unit tests?</h4>
              <p>
                Ans : it is a software testing technique . it is a individual
                units of software. And it is i.e. group of computer program
                modules. its tested to determine whether they are suitable or
                they are not not suitable to use.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Blogs;
