import React from "react";
import { Container, Nav, Navbar } from "react-bootstrap";
import { useAuthState } from "react-firebase-hooks/auth";
import { Link } from "react-router-dom";
import logo from "../../assets/img/logo.png";
import auth from "../../firebase.init";
import { signOut } from "firebase/auth";
const Header = () => {
  const [user, loading, error] = useAuthState(auth);
  const logout = () => {
    signOut(auth);
    localStorage.removeItem("accessToken");
  };
  console.log(user?.displayName);
  console.log(user);
  if (loading) {
    return <p>loading</p>;
  }
  return (
    <Navbar
      collapseOnSelect
      className="header_area"
      expand="lg"
      bg="light"
      variant="light"
    >
      <Container fluid>
        <Link to="/">
          <img src={logo} alt="" />
        </Link>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse
          className="main_menu justify-content-end"
          id="responsive-navbar-nav "
        >
          <Nav className="align-items-center">
            <Link className="menu_item" to="/">
              home
            </Link>
            <Link className="menu_item" to="/my_portfolio">
              my portfolio
            </Link>
            <Link className="menu_item" to="/blogs">
              Blogs
            </Link>
            <Link className="menu_item" to="/parchage">
              pasrcase
            </Link>
            <Link className="menu_item" to="/dashboard/myProfile">
              dashboard
            </Link>
            {user ? (
              <div className="d-flex align-items-center gap-2">
                <p className="menu_item">{user?.displayName}</p>
                <button
                  onClick={logout}
                  className="primary_btn3 small_btn radius_5px"
                >
                  Logout
                </button>
              </div>
            ) : (
              <Link to="/login" className="primary_btn3 small_btn radius_5px">
                login
              </Link>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default Header;
