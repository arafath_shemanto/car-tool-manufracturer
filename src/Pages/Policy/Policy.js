import React from "react";
import return_icon from "../../assets/img/svg/return.svg";
import Shipping from "../../assets/img/svg/Shipping.svg";
import Warranty from "../../assets/img/svg/Warranty.svg";
const Policy = () => {
  return (
    <div className="plicy_section home17_ReturnsShop_area m-0">
      <div className="container">
        <div className="row">
          <div className="col-lg-4 col-md-6">
            <div className="home17_info_box text-center mb_30">
              <div className="icon text-center">
                <img src={return_icon} alt="" />
              </div>
              <h4>30 Days Returns</h4>
              <p>
                Since 1991, Car Store has been a purveyor quality modern
                furniture ohio eith 20 international
              </p>
            </div>
          </div>
          <div className="col-lg-4 col-md-6">
            <div className="home17_info_box text-center mb_30">
              <div className="icon text-center">
                <img src={Shipping} alt="" />
              </div>
              <h4>Free Shipping</h4>
              <p>
                Since 1991, Car Store has been a purveyor quality modern
                furniture ohio eith 20 international
              </p>
            </div>
          </div>
          <div className="col-lg-4 col-md-6">
            <div className="home17_info_box text-center mb_30">
              <div className="icon text-center">
                <img src={Warranty} alt="" />
              </div>
              <h4>International Warranty</h4>
              <p>
                Since 1991, Car Store has been a purveyor quality modern
                furniture ohio eith 20 international
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Policy;
