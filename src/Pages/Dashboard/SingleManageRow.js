import React from "react";
import { confirm } from "react-bootstrap-confirmation";
// console.log(order, index);
// const { part_name, unit_prise, quentity_number, _id } = order;

const SingleManageRow = ({ product, refetch, index }) => {
  console.log(product);
  const { name, single_prise, available_quantity, _id } = product;
  console.log(product);
  const deleteHandeler = async (_id) => {
    console.log(_id);
    const confrimDelete = await confirm("Want to delete your order?");
    if (confrimDelete) {
      console.log(`dta delte id is ${_id}`);
      fetch(`https://mighty-peak-09246.herokuapp.com/delete_parts/${_id}`, {
        method: "DELETE",
      })
        .then((res) => res.json())
        .then((data) => {
          refetch();
          console.log(data);
        });
    }
  };
  return (
    <tr>
      <td>{index + 1}</td>
      <td>{name}</td>
      <td>{single_prise}</td>
      <td>{available_quantity}</td>
      <td>
        <button
          className="primary_btn3 small_btn radius_5px me-2"
          onClick={() => deleteHandeler(_id)}
        >
          Delete Product
        </button>
      </td>
    </tr>
  );
};

export default SingleManageRow;
