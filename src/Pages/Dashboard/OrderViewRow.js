import React from "react";
import { confirm } from "react-bootstrap-confirmation";

const OrderViewRow = ({ order, refetch, index }) => {
  console.log(order);
  const { part_name, email, quentity_number } = order;
  console.log(order);
  return (
    <tr>
      <td>{index + 1}</td>
      <td>{part_name}</td>
      <td>{email}</td>
      <td>{quentity_number}</td>
    </tr>
  );
};

export default OrderViewRow;
