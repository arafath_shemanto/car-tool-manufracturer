import React from "react";
import { Table } from "react-bootstrap";
import { useQuery } from "react-query";
import UserRowSingle from "./UserRowSingle";

const Users = () => {
  const {
    data: users,
    isLoading,
    refetch,
  } = useQuery("users", () =>
    fetch(`https://mighty-peak-09246.herokuapp.com/users`, {
      method: "GET",
      headers: {
        authorization: `Bearer ${localStorage.getItem("accessToken")}`,
      },
    }).then((res) => res.json())
  );
  if (isLoading) {
    return <p>loading</p>;
  }
  return (
    <div>
      <h3>users</h3>
      {users?.length}
      <div className="container">
        <Table responsive>
          <thead>
            <tr>
              <th>No</th>
              <th>Email</th>
              <th>Role</th>
            </tr>
          </thead>
          <tbody>
            {users.map((item, index) => (
              <UserRowSingle
                key={index}
                refetch={refetch}
                item={item}
                index={index}
              ></UserRowSingle>
            ))}
          </tbody>
        </Table>
      </div>
    </div>
  );
};

export default Users;
