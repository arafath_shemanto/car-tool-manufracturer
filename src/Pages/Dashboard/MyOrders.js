import React, { useEffect, useState } from "react";
import { Table } from "react-bootstrap";
import { useAuthState } from "react-firebase-hooks/auth";
import { Navigate, useNavigate } from "react-router-dom";
import auth from "../../firebase.init";
import { signOut } from "firebase/auth";
import { useQuery } from "react-query";
import OrderRow from "./OrderRow";
const MyOrders = () => {
  // const [orders, setOrders] = useState([]);
  const [user] = useAuthState(auth);
  const navigate = useNavigate();

  // useEffect(() => {
  //   if (user) {
  //     const url = `https://mighty-peak-09246.herokuapp.com/get-orders?email=${user.email}`;
  //     fetch(url, {
  //       method: "GET",
  //       headers: {
  //         authorization: `Bearer ${localStorage.getItem("accessToken")}`,
  //       },
  //     })
  //       .then((res) => {
  //         if (res.status === 401 || res.status === 403) {
  //           navigate("/");
  //           signOut(auth);
  //           localStorage.removeItem("accessToken");
  //         }
  //         return res.json();
  //       })
  //       .then((data) => setOrders(data));
  //   }
  // }, [user]);
  const {
    data: orders,
    isLoading,
    refetch,
  } = useQuery("products", () =>
    fetch(
      `https://mighty-peak-09246.herokuapp.com/get-orders?email=${user?.email}`,
      {
        method: "GET",
        headers: {
          authorization: `Bearer ${localStorage.getItem("accessToken")}`,
        },
      }
    ).then((res) => {
      if (res.status === 401 || res.status === 403) {
        navigate("/");
        signOut(auth);
        localStorage.removeItem("accessToken");
      }
      return res.json();
    })
  );

  if (isLoading) {
    return <p>loading</p>;
  }
  return (
    <div className="table-responsive">
      <h3 class="text-capitalize mb-3 mt-2">my orders {orders?.length}</h3>
      <Table striped bordered hover responsive>
        <thead>
          <tr>
            <th>No</th>
            <th>Order Name</th>
            <th>Prise</th>
            <th>quentity</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {orders.map((order, index) => (
            <OrderRow
              refetch={refetch}
              index={index}
              key={index}
              order={order}
            ></OrderRow>
          ))}
        </tbody>
      </Table>
    </div>
  );
};

export default MyOrders;
