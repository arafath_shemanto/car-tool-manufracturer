import React, { useEffect, useState } from "react";
import { Table } from "react-bootstrap";
import { useAuthState } from "react-firebase-hooks/auth";
import { Navigate, useNavigate } from "react-router-dom";
import auth from "../../firebase.init";
import { signOut } from "firebase/auth";
import { useQuery } from "react-query";
import SingleManageRow from "./SingleManageRow";
const ManageProduct = () => {
  const [user] = useAuthState(auth);
  const navigate = useNavigate();
  const {
    data: products,
    isLoading,
    refetch,
  } = useQuery("products", () =>
    fetch(`https://mighty-peak-09246.herokuapp.com/get-parts`, {
      method: "GET",
      headers: {
        authorization: `Bearer ${localStorage.getItem("accessToken")}`,
      },
    }).then((res) => {
      if (res.status === 401 || res.status === 403) {
        navigate("/");
        signOut(auth);
        localStorage.removeItem("accessToken");
      }
      return res.json();
    })
  );

  if (isLoading) {
    return <p>loading</p>;
  }
  return (
    <div className="table-responsive">
      <h3 class="text-capitalize mb-3 mt-2">
        Total products {products?.length}
      </h3>
      <Table striped bordered hover responsive>
        <thead>
          <tr>
            <th>No</th>
            <th>Product Name</th>
            <th>Prise</th>
            <th>quentity</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {products.map((product, index) => (
            <SingleManageRow
              product={product}
              refetch={refetch}
              key={index}
              index={index}
            ></SingleManageRow>
          ))}
        </tbody>
      </Table>
    </div>
  );
};

export default ManageProduct;
