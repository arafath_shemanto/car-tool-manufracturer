import React, { useEffect, useState } from "react";
import { Table } from "react-bootstrap";
import { useAuthState } from "react-firebase-hooks/auth";
import { Navigate, useNavigate } from "react-router-dom";
import auth from "../../firebase.init";
import { signOut } from "firebase/auth";
import { useQuery } from "react-query";
import OrderViewRow from "./OrderViewRow";
const ManageOrders = () => {
  const [user] = useAuthState(auth);
  const navigate = useNavigate();
  const {
    data: Orders,
    isLoading,
    refetch,
  } = useQuery("Orders", () =>
    fetch(`https://mighty-peak-09246.herokuapp.com/total_orders`, {
      method: "GET",
      headers: {
        authorization: `Bearer ${localStorage.getItem("accessToken")}`,
      },
    }).then((res) => {
      if (res.status === 401 || res.status === 403) {
        navigate("/");
        signOut(auth);
        localStorage.removeItem("accessToken");
      }
      return res.json();
    })
  );

  if (isLoading) {
    return <p>loading</p>;
  }
  return (
    <div>
      {/* /total_orders  */}
      <h3>manage orders</h3>
      <div className="table-responsive">
        <h3 class="text-capitalize mb-3 mt-2">Total orders {Orders?.length}</h3>
        <Table striped bordered hover responsive>
          <thead>
            <tr>
              <th>No</th>
              <th>Product Name</th>
              <th>Email</th>
              <th>quentity</th>
            </tr>
          </thead>
          <tbody>
            {Orders?.map((order, index) => (
              <OrderViewRow
                key={index}
                index={index}
                order={order}
              ></OrderViewRow>
            ))}
          </tbody>
        </Table>
      </div>
    </div>
  );
};

export default ManageOrders;
