import React from "react";

const UserRowSingle = ({ item, refetch, index }) => {
  console.log(item);
  const { email, role } = item;
  //   console.log(role);
  const makeAdminHandeler = (email) => {
    console.log(email);
    fetch(`https://mighty-peak-09246.herokuapp.com/users/admin/${email}`, {
      method: "PUT",
      headers: {
        authorization: `Bearer ${localStorage.getItem("accessToken")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        refetch();
        console.log(data);
      });
  };

  return (
    <tr>
      <td>{index + 1}</td>
      <td>{email}</td>
      <td>
        {role !== "admin" ? (
          <button
            className="primary_btn3 small_btn radius_5px"
            onClick={() => makeAdminHandeler(email)}
          >
            make admin
          </button>
        ) : (
          <button className="primary_btn small_btn radius_5px">
            Already admin
          </button>
        )}
      </td>
    </tr>
  );
};

export default UserRowSingle;
