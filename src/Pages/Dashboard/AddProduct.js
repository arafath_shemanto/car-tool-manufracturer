import React, { useState } from "react";
import { Button } from "react-bootstrap";

const AddProduct = () => {
  const submitHandeler = (e) => {
    e.preventDefault();
    const name = e.target.name.value;
    const img = e.target.img.value;
    const description = e.target.description.value;
    const minimum_order = e.target.minimum_order.value;
    const available_quantity = e.target.available_quantity.value;
    const single_prise = e.target.available_quantity.value;
    const product = {
      name,
      img,
      description,
      minimum_order,
      available_quantity,
      single_prise,
      single_prise,
    };
    console.log(product);
    const url = `https://mighty-peak-09246.herokuapp.com/add-parts`;
    fetch(url, {
      method: "POST",
      headers: {
        "content-type": "application/json",
      },
      body: JSON.stringify(product),
    })
      .then((res) => res.json())
      .then((result) => console.log(result));
    e.target.reset();
  };
  return (
    <div className="section_spacing">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className="theme_title">
              <h3>Add a Product</h3>
            </div>
          </div>
          <div className="col-12">
            <form onSubmit={submitHandeler}>
              <div className="col-12 mb_20">
                <label className="primary_label2">Product name</label>
                <input
                  name="name"
                  placeholder="name"
                  className="primary_input4 radius_5px"
                  type="text"
                  required
                />
              </div>
              <div className="col-12 mb_20">
                <label className="primary_label2">Product img</label>
                <input
                  name="img"
                  placeholder="img"
                  className="primary_input4 radius_5px"
                  type="text"
                  required
                />
              </div>
              <div className="col-12 mb_20">
                <label className="primary_label2">description</label>
                <input
                  name="description"
                  placeholder="description"
                  className="primary_input4 radius_5px"
                  type="text"
                  required
                />
              </div>
              <div className="col-12 mb_20">
                <label className="primary_label2">minimum order</label>
                <input
                  name="minimum_order"
                  placeholder="minimum order"
                  className="primary_input4 radius_5px"
                  type="number"
                  required
                />
              </div>
              <div className="col-12 mb_20">
                <label className="primary_label2">available quantity</label>
                <input
                  name="available_quantity"
                  placeholder="quantity"
                  className="primary_input4 radius_5px"
                  type="number"
                  required
                />
              </div>
              <div className="col-12 mb_20">
                <label className="primary_label2">single prise</label>
                <input
                  name="single_prise"
                  placeholder="prise"
                  className="primary_input4 radius_5px"
                  type="number"
                  required
                />
              </div>
              <div className="col-12 mb_20">
                <Button type="submit" className="primary_btn3">
                  submit product
                </Button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddProduct;
