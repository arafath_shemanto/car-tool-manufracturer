import React from "react";
import { ListGroup } from "react-bootstrap";
import { useAuthState } from "react-firebase-hooks/auth";
import { Link, Outlet } from "react-router-dom";
import auth from "../../firebase.init";
import useAdminhook from "../../hooks/useAdminhook";

const Dashboard = () => {
  const [user] = useAuthState(auth);
  const [admin] = useAdminhook(user);
  return (
    <div className="section_spacing">
      <div className="container-fluid">
        <div className="row">
          <div className="col-lg-3 ">
            <div className="sidebar_menu mb-5">
              {!admin && (
                <>
                  {" "}
                  <Link to="/dashboard/myorders">My Orders</Link>
                  <Link to="/dashboard/myReview">My Review</Link>
                </>
              )}

              <Link to="/dashboard/myProfile">My Profile</Link>
              {admin && (
                <>
                  <Link to="/dashboard/users">All users</Link>
                  <Link to="/dashboard/addProduct">add Product</Link>
                  <Link to="/dashboard/manageProducts">All Products</Link>
                  <Link to="/dashboard/manageOrders">All Orders</Link>
                </>
              )}
            </div>
          </div>
          <div className="col-lg-9">
            <h3 className="text-uppercase">dashboard</h3>
            <Outlet />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
