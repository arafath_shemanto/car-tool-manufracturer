import React, { useState } from "react";
import { Button } from "react-bootstrap";

const MyReview = () => {
  const [ratingError, setRatingError] = useState("");
  let errorRating;
  const ratingHandeler = (e) => {
    const inputRating = e.target.value;
    if (inputRating > 5 || inputRating < 0) {
      setRatingError("Rating must be 1 to 5");
    } else {
      setRatingError("");
    }
    console.log(ratingError);
  };
  const reviewHandeler = (e) => {
    e.preventDefault();
    const description = e.target.description.value;
    const ratings = e.target.ratings.value;
    console.log(description, ratings);
    const reviews = { description, ratings };
    console.log(reviews);
    const url = `https://mighty-peak-09246.herokuapp.com/add-review`;
    fetch(url, {
      method: "POST",
      headers: {
        "content-type": "application/json",
      },
      body: JSON.stringify(reviews),
    })
      .then((res) => res.json())
      .then((result) => console.log(result));
    e.target.reset();
  };
  return (
    <div className="section_spacing">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className="theme_title">
              <h3>My Review</h3>
            </div>
          </div>
          <div className="col-12">
            <form onSubmit={reviewHandeler}>
              <div className="col-12 mb_20">
                <label className="primary_label2">Description</label>
                <textarea
                  type="text"
                  name="description"
                  placeholder="Enter Your Description"
                  className="primary_textarea4 radius_5px"
                  required
                />
              </div>
              <div className="col-12 mb_20">
                <label className="primary_label2">your ratings</label>
                <input
                  name="ratings"
                  placeholder="Enter Your Description"
                  className="primary_input4 radius_5px"
                  type="number"
                  required
                  onChange={ratingHandeler}
                />
                <p>{ratingError && ratingError}</p>
              </div>
              <div className="col-12 mb_20">
                <Button
                  disabled={ratingError ? true : false}
                  type="submit"
                  className="primary_btn3"
                >
                  Submit review
                </Button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MyReview;
