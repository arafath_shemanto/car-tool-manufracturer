import React from "react";
import { Button } from "react-bootstrap";
import { useAuthState } from "react-firebase-hooks/auth";
import auth from "../../firebase.init";

const My_profile = () => {
  const [user, loading, error] = useAuthState(auth);
  if (loading) {
    return <p>loading</p>;
  }
  const submitHandeler = (e) => {
    e.preventDefault();
    const userName = e.target.userName.value;
    const email = e.target.email.value;
    const education = e.target.education.value;
    const phone = e.target.phone.value;
    const LinkedIn = e.target.LinkedIn.value;
    const data = {
      userName,
      email,
      education,
      phone,
      LinkedIn,
    };
    const url = `https://mighty-peak-09246.herokuapp.com/profile?email=${user?.email}`;
    fetch(url, {
      method: "PUT",
      headers: {
        "content-type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((result) => console.log(result));
    e.target.reset();
  };
  return (
    <div>
      <h3 className="mt-4 mb-4">My Profile</h3>
      <div className="container">
        <div className="row">
          <div className="col-lg-6">
            <form onSubmit={submitHandeler}>
              <div className="col-12 mb_20">
                <label className="primary_label2">Name</label>
                <input
                  name="userName"
                  disabled
                  placeholder="Enter user name or email"
                  className="primary_input3 radius_5px "
                  value={user?.displayName}
                />
              </div>
              <div className="col-12 mb_20">
                <label className="primary_label2">Email</label>
                <input
                  name="email"
                  type="email"
                  disabled
                  value={user?.email}
                  placeholder="Enter user name or email"
                  className="primary_input3 radius_5px "
                />
              </div>
              <div className="col-12 mb_20">
                <label className="primary_label2">education</label>
                <input
                  name="education"
                  type="text"
                  required
                  placeholder="Enter education"
                  className="primary_input3 radius_5px "
                />
              </div>
              <div className="col-12 mb_20">
                <label className="primary_label2">Location</label>
                <input
                  name="location"
                  type="text"
                  required
                  placeholder="Enter Location"
                  className="primary_input3 radius_5px "
                />
              </div>
              <div className="col-12 mb_20">
                <label className="primary_label2">phone number</label>
                <input
                  name="phone"
                  type="text"
                  required
                  placeholder="Enter phone number"
                  className="primary_input3 radius_5px "
                />
              </div>
              <div className="col-12 mb_20">
                <label className="primary_label2">LinkedIn profile link</label>
                <input
                  name="LinkedIn"
                  type="text"
                  required
                  placeholder="LinkedIn profile link"
                  className="primary_input3 radius_5px "
                />
              </div>
              <div className="col-12">
                <Button
                  type="submit"
                  className="primary_btn3 min_200 radius_5px  "
                >
                  Update Profile
                </Button>
              </div>
            </form>
          </div>
          <div className="col-lg-6"></div>
        </div>
      </div>
    </div>
  );
};

export default My_profile;
