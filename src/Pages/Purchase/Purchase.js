import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useAuthState } from "react-firebase-hooks/auth";
import auth from "../../firebase.init";
import { Button } from "react-bootstrap";
const Purchase = () => {
  const { id } = useParams();

  const [user, loading, error] = useAuthState(auth);
  const [quentitError, setQuentitError] = useState("");
  console.log(user);
  console.log(id);
  const [part, setPart] = useState({});
  const url = `https://mighty-peak-09246.herokuapp.com/get-parts/${id}`;
  useEffect(() => {
    fetch(url)
      .then((res) => res.json())
      .then((data) => setPart(data));
  }, [url]);
  const {
    _id,
    name,
    img,
    description,
    minimum_order,
    single_prise,
    available_quantity,
  } = part;
  if (loading) {
    return <p>loading</p>;
  }
  const quentityhandeler = (e) => {
    const inputQuentity = e.target.value;
    if (inputQuentity < minimum_order || inputQuentity > available_quantity) {
      setQuentitError(
        `Quentity must be between ${minimum_order} to ${available_quantity} pcs`
      );
    } else {
      setQuentitError(``);
    }
  };

  const onSubmit = (e) => {
    e.preventDefault();
    const part_name = e.target.partsName.value;
    const email = e.target.email.value;
    const address = e.target.address.value;
    const phone = e.target.phone.value;
    const quentity_number = e.target.quentity_number.value;
    const unit_prise = e.target.perunitPrise.value;
    console.log(email, address, phone, quentity_number);
    const data = {
      part_name,
      email,
      address,
      phone,
      quentity_number,
      unit_prise,
    };
    const url = `https://mighty-peak-09246.herokuapp.com/post-orders`;
    fetch(url, {
      method: "POST",
      headers: {
        "content-type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((result) => console.log(result));
    e.target.reset();
  };
  return (
    <div className="parchase_section section_spacing">
      <div className="container">
        <div className="row">
          <div className="col-12 mb-3">
            <div className="theme_title">
              <h3>purchase page</h3>
            </div>
            <img src={img} alt="" className="img-fluid" />
            <p>
              Your Parts id : <b>{_id}</b>{" "}
            </p>
            <p>
              Your Parts Name: <b>{name}</b>
            </p>
            <p>Your Parts description : {description}</p>
          </div>
          <div className="col-xl-6">
            <form onSubmit={onSubmit}>
              <div className="col-12 mb_20">
                <label className="primary_label2">Name</label>
                <input
                  name="name"
                  disabled
                  placeholder="Enter user name or email"
                  className="primary_input3 radius_5px "
                  value={user?.displayName}
                />
              </div>
              <div className="col-12 mb_20">
                <label className="primary_label2">Email</label>
                <input
                  name="email"
                  type="email"
                  disabled
                  value={user?.email}
                  placeholder="Enter user name or email"
                  className="primary_input3 radius_5px "
                />
              </div>
              <div className="col-12 mb_20">
                <label className="primary_label2">parts name</label>
                <input
                  name="partsName"
                  type="text"
                  required
                  disabled
                  value={name}
                  className="primary_input3 radius_5px "
                />
              </div>
              <div className="col-12 mb_20">
                <label className="primary_label2">unit Prise</label>
                <input
                  name="perunitPrise"
                  type="text"
                  required
                  disabled
                  value={single_prise}
                  className="primary_input3 radius_5px "
                />
              </div>
              <div className="col-12 mb_20">
                <label className="primary_label2">address</label>
                <input
                  name="address"
                  type="text"
                  required
                  placeholder="type your address"
                  className="primary_input3 radius_5px "
                />
              </div>
              <div className="col-12 mb_20">
                <label className="primary_label2">phone number,</label>
                <input
                  name="phone"
                  type="text"
                  required
                  placeholder="type your phone number"
                  className="primary_input3 radius_5px "
                />
              </div>
              <div className="col-12 mb_20">
                <label className="primary_label2">
                  Product Quentity . minnimum order must be : {minimum_order}
                  pics
                </label>
                <input
                  name="quentity_number"
                  type="number"
                  placeholder="order quentity"
                  required
                  onChange={quentityhandeler}
                  defaultValue={minimum_order}
                  className="primary_input3 radius_5px"
                />
                <p>{quentitError && quentitError}</p>
              </div>
              <div className="col-12">
                <Button
                  disabled={quentitError ? true : false}
                  type="submit"
                  className="primary_btn3 min_200 radius_5px  "
                >
                  place the order
                </Button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Purchase;
