import React from "react";

import blog_img_1 from "../../assets/img/blog/1.jpg";
import blog_img_2 from "../../assets/img/blog/2.jpg";
import blog_img_3 from "../../assets/img/blog/3.jpg";

const Blog = () => {
  return (
    <div className="home22_padding section_spacing ">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className="theme_title text-start mb_40">
              <h3>From Our Blog Mobile Parts</h3>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-4 col-md-6">
            <div className="home22_blog_Widget mb_30">
              <a href="#" className="thumb">
                <img className="img-fluid" src={blog_img_1} alt="" />
              </a>
              <div className="blog_content">
                <span>
                  April 08, 2024 <span className="dot_devide m-0"> • </span>{" "}
                  Mobile Parts Coming
                </span>
                <a href="#">
                  <h4>
                    thinking about lounch our new parts we are going to lounch
                    mobile parts .
                  </h4>
                </a>
                <a
                  href="#"
                  className="cirle_readMore_btn d-flex align-items-center gap_10"
                >
                  <h5 className="font_14 f_w_700 text-uppercase m-0">
                    Read More
                  </h5>
                </a>
              </div>
            </div>
          </div>
          <div className="col-lg-4 col-md-6">
            <div className="home22_blog_Widget mb_30">
              <a href="#" className="thumb">
                <img className="img-fluid" src={blog_img_2} alt="" />
              </a>
              <div className="blog_content">
                <span>
                  April 08, 2024 <span className="dot_devide m-0"> • </span>{" "}
                  Mobile Parts Coming
                </span>
                <a href="#">
                  <h4>
                    thinking about lounch our new parts we are going to lounch
                    mobile parts .
                  </h4>
                </a>
                <a
                  href="#"
                  className="cirle_readMore_btn d-flex align-items-center gap_10"
                >
                  <h5 className="font_14 f_w_700 text-uppercase m-0">
                    Read More
                  </h5>
                </a>
              </div>
            </div>
          </div>
          <div className="col-lg-4 col-md-6">
            <div className="home22_blog_Widget mb_30">
              <a href="#" className="thumb">
                <img className="img-fluid" src={blog_img_3} alt="" />
              </a>
              <div className="blog_content">
                <span>
                  April 08, 2024 <span className="dot_devide m-0"> • </span>{" "}
                  Mobile Parts Coming
                </span>
                <a href="#">
                  <h4>
                    thinking about lounch our new parts we are going to lounch
                    mobile parts .
                  </h4>
                </a>
                <a
                  href="#"
                  className="cirle_readMore_btn d-flex align-items-center gap_10"
                >
                  <h5 className="font_14 f_w_700 text-uppercase m-0">
                    Read More
                  </h5>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Blog;
