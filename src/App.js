import { Routes, Route } from "react-router-dom";
import Home from "./Pages/Home/Home";
import Header from "./Pages/Shared/Header";
import Footer from "./Pages/Shared/Footer";
import Purchase from "./Pages/Purchase/Purchase";
import Dashboard from "./Pages/Dashboard/Dashboard";
import Blog from "./Pages/Blog/Blog";
import Login from "./Pages/Login/Login";
import Register from "./Pages/Login/Register";
import RequireAuth from "./Pages/Login/RequireAuth";
import Error_page from "./Pages/Shared/Error_page";
import Blogs from "./Pages/Blogs/Blogs";
import MyPortfolio from "./Pages/Portfolio/MyPortfolio";
import MyOrders from "./Pages/Dashboard/MyOrders";
import My_profile from "./Pages/Dashboard/My_profile";
import MyReview from "./Pages/Dashboard/MyReview";
import Users from "./Pages/Dashboard/Users";
import ManageProduct from "./Pages/Dashboard/ManageProduct";
import ManageOrders from "./Pages/Dashboard/ManageOrders";
import AddProduct from "./Pages/Dashboard/AddProduct";
function App() {
  return (
    <div className="App">
      <Header></Header>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route
          path="parchage/:id"
          element={
            <RequireAuth>
              <Purchase />
            </RequireAuth>
          }
        ></Route>
        <Route
          path="parchage"
          element={
            <RequireAuth>
              <Purchase />
            </RequireAuth>
          }
        ></Route>
        <Route
          path="dashboard"
          element={
            <RequireAuth>
              <Dashboard />
            </RequireAuth>
          }
        >
          <Route path="myorders" element={<MyOrders></MyOrders>}></Route>
          <Route
            index
            path="myProfile"
            element={<My_profile></My_profile>}
          ></Route>
          <Route path="myReview" element={<MyReview></MyReview>}></Route>
          {/* admin  */}
          <Route path="users" element={<Users></Users>}></Route>
          <Route path="addProduct" element={<AddProduct></AddProduct>}></Route>
          <Route
            path="manageProducts"
            element={<ManageProduct></ManageProduct>}
          ></Route>
          <Route
            path="manageOrders"
            element={<ManageOrders></ManageOrders>}
          ></Route>
        </Route>
        <Route path="blog" element={<Blog />}></Route>
        <Route path="login" element={<Login />}></Route>
        <Route path="register" element={<Register />}></Route>
        <Route path="blogs" element={<Blogs />}></Route>
        <Route path="my_portfolio" element={<MyPortfolio />}></Route>
        <Route path="*" element={<Error_page />}></Route>
      </Routes>
      <Footer></Footer>
    </div>
  );
}

export default App;
